<?php
require_once 'src/FrequencyAnalysis.php';

echo 'Single Responsibility Principle'."\n\n";

$article = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'slovak_spectator.txt');

$analysis = new Matok\Crypto\Analysis\LetterFrequency();

echo 'Frequency analysis of article:'."\n";
$analysis->analyse($article);
$analysis->printFrequency();