# Lecture: Single Resposibility Principle

Examine **LetterFrequency** (simple implementation of https://en.wikipedia.org/wiki/Letter_frequency)

Run:
```
php app.php
```

## Task
1. Mix different levels of abstraction - find the problem and rewrite it in order to each method has same level of abstraction. (SLA)
2. One method probably does not belong to this class - remove it. (SRP) Hint: modify app.php
3. Extra: methods count: finish with one public method, 3 private methods (better readability, maintanance)
4. Extra: new class: Create "string util monostate class" to remove whitespaces, quotes... (SRP)