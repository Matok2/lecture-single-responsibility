<?php
 
namespace Matok\Crypto\Analysis;

/**
 * https://en.wikipedia.org/wiki/Letter_frequency
 *
 * This class analyse letter frequency of text. But it has several problems:
 * 1) Mix different levels of abstraction - find the problem and rewrite it in order to each method has same level of abstraction. (SLA)
 * 2) One method probably does not belong to this class - remove it. (SR) Hint: modify app.php
 * 3) Bonus A) methods count: finish with one public method, 3 private methods
 * 4) Bonus B) new class: Create "string util monostate class" to remove whitespaces, quotes... (SR)
 */
class LetterFrequency
{
    private $chars;
    private $count;
 
    public function analyse($text)
    {
        $text = preg_replace('#\s+#', '', $text);

        $chars = str_split($text, 1);

        foreach ($chars as $char) {
            $this->count++;

            if (!isset($this->chars[$char])) {
                $this->chars[$char] = 0;
            }

            $this->chars[$char] ++;
        }

        $this->computeRelativeFrequencies();
    }
 
    public function printFrequency()
    {
        foreach ($this->chars as $char => $frequency) {
            printf("%s %0.2f\n", $char, $frequency);
        }
    }
 
    private function computeRelativeFrequencies()
    {
        foreach ($this->chars as $char => $total) {
            $this->chars[$char] = $total / $this->count;
        }
    }
}
