Slovakia gets its first medal at winter Olympics: biathlete Kuzmina wins silver
After a disappointing start on February 10, Anastasiya Kuzmina won silver in an exciting pursuit race.

Despite having to do four extra rounds, Slovak biathlete Anastasiya Kuzmina came second in the 10-kilometres pursuit race at the Winter Olympics in Peyong-Chang on February 12.

The clear winner was German, Laura Dahlmeier and third, just 2 tenths of a second behind Kuzmina was France’s Anais Bescond, the Sme daily wrote.

Another Slovak biathlete, Paulína Fialková, came 38th.

Bad start, good finish
“Thank you for still liking me,” Kuzmina said after her triumph, commenting on her failed sprint race on February 10.

She could have fared better also on Monday but she missed four targets and had to do four extra rounds.

“I came here to repeat a golden victory in the sprint… but in the end, I repeated the silver from the pursuit race in Vancouver…” Kuzmina commented, as quoted by Sme.

Currently, she holds two gold (from Vancouver and Sochi) and two silver Olympic medals.

“I am unbelievably thankful and happy,” the Slovak biathlete said for the public-service TV broadcaster RTVS, adding that she put everything into the race and she hopes to have brought joy to Slovak audiences.

Kuzmina faces two more individual races and two relay races in Peyong-Chang. She believes she is again in the run for medals.

“There is still time left,” she commented for RTVS. “The Olympics are just beginning.”
